import express from "express";
import connectDB from "./config/db.js";
import userRouter from "./routes/User.route.js";
import authCheck from "./middlewares/auth.middleware.js";
import marketplace_inventoryRouter from "./routes/Marketplace_inventory.route.js";
import oemSpecRouter from "./routes/OEMSpec.route.js";
import * as fs from "fs";
var readJSONDump = async (path) => { return new Promise((resolve, reject) => { fs.readFile(path, (e, d) => { if (e) {
    reject(e);
}
else
    resolve(JSON.parse(d.toString())); }); }); };
import cors from "cors";
import dotenv from 'dotenv';
dotenv.config();
const app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// Enable CORS
app.use(cors());
// Body parser
app.use(express.json());
app.get('/', async (req, res) => {
    try {
        res.status(200).send({ message: 'Welcome to the homepage' });
    }
    catch (error) {
        console.log('Error:', error);
        res.status(500).send({ message: 'Internal server error!', error });
    }
});
// user routes
app.use("/user", userRouter);
// Authentication middleware
app.use(authCheck);
// Marketplace routes
app.use("/marketplace", marketplace_inventoryRouter);
// OEM-spec routes
app.use("/oemspec", oemSpecRouter);
// Wrong endpoint URL
app.use('*', async (req, res) => {
    res.sendStatus(422);
});
// App listener
app.listen(process.env.PORT || 7482, async () => {
    try {
        console.log(`Server is running on http://localhost:${process.env.PORT || 7482}`);
        console.log('⏳ Database connecting...');
        await connectDB;
        console.log('✅ Database connected.');
    }
    catch (error) {
        console.log('❌ Error:', error);
    }
});
/*
import { Schema, model } from 'mongoose';
import * as mongoose from "mongoose";
const oemSpecSchema = new Schema({
     brand: {
          type: String,
          required: true,
     },
     car_model: {
          type: String,
          required: true,
     },
     year: {
          type: Number,
          required: true,
     },
     listPrice: {
          type: Number,
          required: true,
     },
     colors: [String],
     mileage: {
          type: Number,
          required: true,
     },
     power: {
          type: Number,
          required: true,
     },
     maxSpeed: {
          type: Number,
          required: true,
     },
}, { versionKey: false, timestamps: true });

const OemSpec = mongoose.model('OemSpec', oemSpecSchema);

// Функция для генерации случайного числа в диапазоне
function getRandomNumber(min, max) {
     return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Функция для генерации случайного элемента из массива
function getRandomElement(array) {
     return array[Math.floor(Math.random() * array.length)];
}
const colors = ['Red', 'Blue', 'Green', 'Black', 'White', 'Silver', 'Gray'];
function extend_oem(iob){

     let o={year: getRandomNumber(2010, 2023),
          listPrice: getRandomNumber(10000, 100000),
          colors: (new Array(getRandomNumber(2,6))).fill("White").map(e=>getRandomElement(colors)),
          mileage: getRandomNumber(0, 200000),
          power: getRandomNumber(100, 500),
          maxSpeed: getRandomNumber(150, 300)}
     Object.keys(o).map(e=>iob[e]=o[e])
     return iob
}
// Функция для генерации и вставки случайного документа
async function insertRandomData() {
     const newOemSpec = new OemSpec()

  //   const newCarData = new marketplace_inventoryModel({ dealer: userId,  });
    // await newCarData.save();

     try {
          await newOemSpec.save();
          console.log('Документ успешно вставлен!');
     } catch (error) {
          console.error('Ошибка при вставке документа:', error);
     }
}
await readJSONDump("D:\\_extratask\\bcmern2\\atb\\all-vehicles-model.json").then(async (j)=>{
     j=Object.setPrototypeOf(j,Object.getPrototypeOf([])).map(e=>{return {brand:e.make,car_model:e.model}});
     for (let i = 0; i < 100 ; i++) {
          let nv=extend_oem(j[i]);
          const newOemSpec = new OemSpec(nv)
          await newOemSpec.save();
     }

})
*/
// Вызов функции для вставки нескольких документов
//# sourceMappingURL=index.js.map