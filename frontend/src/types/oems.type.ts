export interface CarData {
    _id: string;
    brand: string;
    car_model: string;
    year: number;
    listPrice: number;
    colors: string[];
    mileage: number;
    power: number;
    maxSpeed: number;
    createdAt: string;
    updatedAt: string;
  }
