const mongoose = import('mongoose');
const oemSpecSchema = new Schema({
    brand: {
        type: String,
        required: true,
    },
    car_model: {
        type: String,
        required: true,
    },
    year: {
        type: Number,
        required: true,
    },
    listPrice: {
        type: Number,
        required: true,
    },
    colors: [String],
    mileage: {
        type: Number,
        required: true,
    },
    power: {
        type: Number,
        required: true,
    },
    maxSpeed: {
        type: Number,
        required: true,
    },
}, { versionKey: false, timestamps: true });

const OemSpec = mongoose.model('OemSpec', oemSpecSchema);

// Функция для генерации случайного числа в диапазоне
function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Функция для генерации случайного элемента из массива
function getRandomElement(array) {
  return array[Math.floor(Math.random() * array.length)];
}

// Массив возможных цветов
const colors = ['Red', 'Blue', 'Green', 'Black', 'White', 'Silver', 'Gray'];

// Массив возможных брендов
const brands = ['Toyota', 'Honda', 'Ford', 'Chevrolet', 'BMW', 'Audi', 'Mercedes-Benz'];

// Функция для генерации и вставки случайного документа
async function insertRandomData() {
  const newOemSpec = new OemSpec({
    brand: getRandomElement(brands),
    car_model: 'Model ' + getRandomNumber(1, 10),
    year: getRandomNumber(2010, 2023),
    listPrice: getRandomNumber(10000, 100000),
    colors: [getRandomElement(colors), getRandomElement(colors)],
    mileage: getRandomNumber(0, 200000),
    power: getRandomNumber(100, 500),
    maxSpeed: getRandomNumber(150, 300),
  });

  try {
    await newOemSpec.save();
    console.log('Документ успешно вставлен!');
  } catch (error) {
    console.error('Ошибка при вставке документа:', error);
  }
}

// Вызов функции для вставки нескольких документов
for (let i = 0; i < 10; i++) {
  insertRandomData();
}

import { Schema, model } from 'mongoose';
import * as mongoose from "mongoose";
const oemSpecSchema = new Schema({
    brand: {
        type: String,
        required: true,
    },
    car_model: {
        type: String,
        required: true,
    },
    year: {
        type: Number,
        required: true,
    },
    listPrice: {
        type: Number,
        required: true,
    },
    colors: [String],
    mileage: {
        type: Number,
        required: true,
    },
    power: {
        type: Number,
        required: true,
    },
    maxSpeed: {
        type: Number,
        required: true,
    },
}, { versionKey: false, timestamps: true });

const OemSpec = mongoose.model('OemSpec', oemSpecSchema);

// Функция для генерации случайного числа в диапазоне
function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Функция для генерации случайного элемента из массива
function getRandomElement(array) {
    return array[Math.floor(Math.random() * array.length)];
}

// Массив возможных цветов
const colors = ['Red', 'Blue', 'Green', 'Black', 'White', 'Silver', 'Gray'];

// Массив возможных брендов
const brands = ['Toyota', 'Honda', 'Ford', 'Chevrolet', 'BMW', 'Audi', 'Mercedes-Benz'];

// Функция для генерации и вставки случайного документа
async function insertRandomData() {
    const newOemSpec = new OemSpec({
        brand: getRandomElement(brands),
        car_model: 'Model ' + getRandomNumber(1, 10),
        year: getRandomNumber(2010, 2023),
        listPrice: getRandomNumber(10000, 100000),
        colors: [getRandomElement(colors), getRandomElement(colors)],
        mileage: getRandomNumber(0, 200000),
        power: getRandomNumber(100, 500),
        maxSpeed: getRandomNumber(150, 300),
    });

    try {
        await newOemSpec.save();
        console.log('Документ успешно вставлен!');
    } catch (error) {
        console.error('Ошибка при вставке документа:', error);
    }
}

// Вызов функции для вставки нескольких документов
for (let i = 0; i < 10; i++) {
    insertRandomData();
}
// Функция для генерации случайного числа в диапазоне
function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Функция для генерации случайного булевого значения
function getRandomBoolean() {
    return Math.random() >= 0.5;
}

// Массив возможных мест регистрации
const registrationPlaces = ['Москва', 'Санкт-Петербург', 'Новосибирск', 'Екатеринбург', 'Казань'];

async function insertRandomCarData() {
    const newCar = new Car({
        carImage: 'http://localhost:5173/src/assets/images/carlogo.png', // Можно заменить на генерацию случайных URL изображений
        odometer: getRandomNumber(0, 200000),
        majorScratches: getRandomNumber(0, 10).toString(),
        originalPaint: getRandomBoolean(),
        noOfAccidents: getRandomNumber(0, 3),
        noOfPreviousBuyers: getRandomNumber(0, 5),
        registrationPlace: getRandomElement(registrationPlaces),
    });

    try {
        await newCar.save();
        console.log('Данные автомобиля успешно вставлены!');
    } catch (error) {
        console.error('Ошибка при вставке данных:', error);
    }
}

// Вставка нескольких записей
for (let i = 0; i < 10; i++) {
    insertRandomCarData();
}
